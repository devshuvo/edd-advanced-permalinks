<?php
/**
 * Plugin Name: EDD Advanced Permalinks
 * Plugin URI: https://plugins.addonmaster.com/edd-advanced-permalinks/
 * Description: The easiest way to manage Easy Digital Downloads Permalinks
 * Author: AddonMaster
 * Author URI: https://addonmaster.com
 * Version: 1.0.2
 * Text Domain: eddap
 * Domain Path: /lang
 * EDD Version Up to: 2.9.x
 */

/**
* Including Plugin file for security
* Include_once
*
* @since 1.0.0
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 *	EDDAP Functions
 */
require_once( dirname( __FILE__ ) . '/inc/eddap-functions.php' );

/**
 *	Plugin Main Class
 */
if ( ! class_exists( 'EDDAP_Advanced_Permalinks' ) ) :
	class EDDAP_Advanced_Permalinks{

		/**
		 * Return edd type slug
		 */
		public function get_eddap_post_type_slug(){
			$structure = get_option( 'eddap-structure' );
			$edd_slug = get_option( 'eddap_post_type_slug' );

			if( $structure == 'post_name' || $structure == 'post_id' ) {
				$edd_slug = str_replace('%post_id%', '', $edd_slug);
				$edd_slug = str_replace('%postname%', '', $edd_slug);
			} else if( $structure == 'post_cat' || $structure == 'post_author' ){
				$edd_slug = str_replace('/%postname%', '', $edd_slug);
			}
			$edd_slug = untrailingslashit( $edd_slug );
			return (get_option( 'eddap_post_type_slug' ) != "") ? $edd_slug : 'downloads';
		}

		/**
		 * It will Return only text from saved slug
		 */
		public function get_eddap_post_type_slug_escaped(){
			$edd_slug = $this->get_eddap_post_type_slug();
			$find = array('%postname%', '%post_id%', '/');
			$edd_slug = str_replace($find, '', $edd_slug);
			return $edd_slug;
		}

		/**
		 * Return archive slug
		 */
		public function get_eddap_archive_slug(){
			return (get_option( 'eddap_archive_slug' ) != "") ? get_option( 'eddap_archive_slug' ) : 'downloads';
		}

		/**
		 * Return category slug
		 */
		public function get_eddap_category_slug(){
			return (get_option( 'eddap_category_slug' ) != "") ? get_option( 'eddap_category_slug' ) : 'downloads/category';
		}

		/**
		 * Return tag slug
		 */
		public function get_eddap_tag_slug(){
			return (get_option( 'eddap_tag_slug' ) != "") ? get_option( 'eddap_tag_slug' ) : 'downloads/tag';
		}

		/**
		 * Constructor
		 */
		public function __construct() {
			// Loaded textdomain
			add_action('plugins_loaded', array( $this, 'plugin_loaded_action' ), 10, 2);
			// Enqueue admin scripts
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_script' ) );
			// Added rewrites rules
			add_filter( 'init', array( $this, 'eddap_rewrites_rules' ));
			// Filter post type link for downloads
			add_filter('post_type_link', array( $this, 'eddap_post_type_link' ), 1, 3);

			// Filter archive/downloads/category/tags
			add_filter( 'edd_download_post_type_args', array( $this, 'set_eddap_post_type_permalink' ));
			add_filter( 'edd_download_category_args', array( $this, 'set_eddap_category_permalink' ));
			add_filter( 'edd_download_tag_args', array( $this, 'set_eddap_tag_permalink' ));

			// Setting fields for permalinks page
			add_action( 'admin_init', array( $this, 'settings_init' ), 10, 1 );
			// Added plugin action link
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'action_links' ) );

			// Flush permalink upon plugin activation/deactivation
			register_activation_hook( __FILE__, array( $this, 'permalink_flush' ) );
			register_deactivation_hook( __FILE__, array( $this, 'permalink_flush' ) );
		}

		/**
		 * Custom Rewrite rule for EDD Type
		 */
		public function eddap_rewrites_rules(){
			$structure = get_option( 'eddap-structure' );
			$esc_edd_slug = $this->get_eddap_post_type_slug_escaped();
			if( $structure == 'post_id' || $structure == 'post_name_id' ) {
		    	global $wp_rewrite;
			    $queryarg = 'post_type=download&p=';
			    $wp_rewrite->add_rewrite_tag( '%post_id%', '([^/]+)', $queryarg );
			    $wp_rewrite->add_permastruct( "{$esc_edd_slug}", "/{$esc_edd_slug}/%post_id%/", false );
		    } else if( $structure == 'post_cat' || $structure == 'post_author' ){
				add_rewrite_rule( 'download/(.*)/(.*)/?$', 'index.php?post_type=download&name=$matches[2]', 'top' );
		    }
		}

		/**
		 * Custom Permalinks for EDD Type
		 */
		public function eddap_post_type_link($post_link, $post = 0, $leavename = false) {
			$structure = get_option( 'eddap-structure' );
			$esc_edd_slug = $this->get_eddap_post_type_slug_escaped();

		    if( $post->post_type == 'download' ) {
		    	global $wp_rewrite;

		    	if( $structure == 'post_id' ) {
		        	return home_url("{$esc_edd_slug}/{$post->ID}/");
		    	} else if( $structure == 'post_name_id' ) {
		    		$post_link = $wp_rewrite->get_extra_permastruct( "{$esc_edd_slug}" );
				    $post_link = str_replace( '%post_id%', "$post->post_name/$post->ID", $post_link );
				    return home_url( user_trailingslashit( $post_link ) );
		    	} else if( $structure == 'post_cat' ) {
				    if ( is_object( $post ) ) {
				        $terms = wp_get_object_terms( $post->ID, 'download_category' );
				        if ( $terms ) {
				            return str_replace( '%download_category%', $terms[0]->slug, $post_link );
				        } else {
				        	return str_replace( '%download_category%', $this->get_eddap_archive_slug(), $post_link );
				        }
				    }
		    	} else if( $structure == 'post_author' ){
		    		$post_author = get_the_author_meta( 'user_login',  $post->post_author );
		    		return str_replace( '%author%', $post_author, $post_link );
		    	}
		    }

		    return $post_link;
		}


		/**
		 * Set EDD and Archive Permalink
		 */
		public function set_eddap_post_type_permalink( $download_args ){
			$edd_slug = $this->get_eddap_post_type_slug();
			$archive_url = $this->get_eddap_archive_slug();

			$download_args['rewrite'] = defined( 'EDD_DISABLE_REWRITE' ) && EDD_DISABLE_REWRITE ? false : array('slug' => $edd_slug, 'with_front' => false);

			$download_args['has_archive'] = defined( 'EDD_DISABLE_ARCHIVE' ) && EDD_DISABLE_ARCHIVE ? false : $archive_url;

			return $download_args;
		}

		/**
		 * Set Category Permalink
		 */
		public function set_eddap_category_permalink( $array ){
			$edd_cat_slug = $this->get_eddap_category_slug();

			$array['rewrite'] = array('slug' => $edd_cat_slug, 'with_front' => true, 'hierarchical' => true );

			return $array;
		}

		/**
		 * Set Tag Permalink
		 */
		public function set_eddap_tag_permalink( $array ){
			$eddap_tag_slug = $this->get_eddap_tag_slug();

			$array['rewrite'] = array( 'slug' => $eddap_tag_slug, 'with_front' => true, 'hierarchical' => true  );

			return $array;
		}

		/**
		 * Admin Setting Initial
		 */
		public function settings_init() {
			self::settings_save();
			add_settings_section( 'eddap-advanced-permalink', esc_html__( 'EDD Advanced Permalinks', 'eddap' ), array( $this, 'settings' ), 'permalink' );
		}

		/**
		 * Show slug input box.
		 */
		public function eddap_post_type_slug_input() {
			?>
			<code><?php echo esc_url( home_url( '/' ) ); ?></code><input name="eddap_post_type_slug" type="text" class="regular-text code" value="<?php echo esc_attr( get_option( 'eddap_post_type_slug' ) ); ?>" placeholder="<?php echo esc_attr_x( 'downloads/%postname%', 'slug', 'eddap' ); ?>" />

			<div class="available-structure-tags">
				<p><?php echo esc_html__( 'Available Structures:','eddap' ); ?></p>
				<ul class="eddap-structure-list" role="list">
					<li>
						<label><input name="eddap-structure" type="radio" value="post_name" <?php checked( 'post_name', esc_attr( get_option( 'eddap-structure' ) ) ); ?>> <?php echo esc_html__( 'Post name', 'eddap' ); ?> <code><?php echo sprintf( __( '%s/item/post-name','eddap' ), esc_url( home_url() ) );?></code></label>
					</li>
					<li>
						<label><input name="eddap-structure" type="radio" value="post_id" <?php checked( 'post_id', esc_attr( get_option( 'eddap-structure' ) ) ); ?> /> <?php echo esc_html__( 'Post ID', 'eddap' ); ?> <code><?php echo sprintf( __( '%s/item/123','eddap' ), esc_url( home_url() ) );?></code></label>
					</li>
					<li>
						<label><input name="eddap-structure" type="radio" value="post_name_id" <?php checked( 'post_name_id', esc_attr( get_option( 'eddap-structure' ) ) ); ?>> <?php echo esc_html__( 'Post name and ID', 'eddap' ); ?> <code><?php echo sprintf( __( '%s/item/post-name/123','eddap' ), esc_url( home_url() ) );?></code></label>
					</li>
					<li>
						<label><input name="eddap-structure" type="radio" value="post_cat" <?php checked( 'post_cat', esc_attr( get_option( 'eddap-structure' ) ) ); ?>> <?php echo esc_html__( 'Category based', 'eddap' ); ?> <code><?php echo sprintf( __( '%s/item/product-category/post-name','eddap' ), esc_url( home_url() ) );?></code></label>
					</li>
					<li>
						<label><input name="eddap-structure" type="radio" value="post_author" <?php checked( 'post_author', esc_attr( get_option( 'eddap-structure' ) ) ); ?>> <?php echo esc_html__( 'Author based', 'eddap' ); ?> <code><?php echo sprintf( __( '%s/item/author/post-name','eddap' ), esc_url( home_url() ) );?></code></label>
					</li>
				</ul>
			</div>
			<?php
		}

		/**
		 * Show archive slug input box.
		 */
		public function eddap_archive_slug_input() {
			?>
			<input name="eddap_archive_slug" type="text" class="regular-text code" value="<?php echo esc_attr( get_option( 'eddap_archive_slug' ) ); ?>" placeholder="<?php echo esc_attr_x( 'downloads', 'slug', 'eddap' ); ?>" />
			<p><code><?php echo sprintf( __( '%s%s/','eddap' ), esc_url( home_url( '/' ) ), esc_attr( $this->get_eddap_archive_slug() ) );?></code></p>
			<?php
		}

		/**
		 * Show category slug input box.
		 */
		public function eddap_category_slug_input() {
			?>
			<input name="eddap_category_slug" type="text" class="regular-text code" value="<?php echo esc_attr( get_option( 'eddap_category_slug' ) ); ?>" placeholder="<?php echo esc_attr_x( 'downloads/category', 'slug', 'eddap' ); ?>" />
			<p><code><?php echo sprintf( __( '%s%s/category-name/','eddap' ), esc_url( home_url( '/' ) ), esc_attr( $this->get_eddap_category_slug() ) );?></code></p>
			<?php
		}

		/**
		 * Show tag slug input box.
		 */
		public function eddap_tag_slug_input() {
			?>
			<input name="eddap_tag_slug" type="text" class="regular-text code" value="<?php echo esc_attr( get_option( 'eddap_tag_slug' ) ); ?>" placeholder="<?php echo esc_attr_x( 'downloads/tag', 'slug', 'eddap' ); ?>" />
			<p><code><?php echo sprintf( __( '%s%s/tag-name/','eddap' ), esc_url( home_url( '/' ) ), esc_attr( $this->get_eddap_tag_slug() ) );?></code></p>
			<?php
		}

		/**
		 * Show the settings panel
		 */
		public function settings() {
			/* translators: %s: Home URL */
			echo wp_kses_post( wpautop( sprintf( __( 'If you like, you may enter custom structures for your product URLs here. For example, using <code>downloads</code> would make your product links like <code>%sitem/sample-product/</code>. If you leave these blank the defaults will be used.', 'eddap' ), esc_url( home_url( '/' ) ) ) ) );
			?>
			<table id="eddap" class="form-table eddap-permalink-structure">
				<tbody>
					<tr>
						<th scope="row"><?php echo esc_html__( 'Download base', 'eddap' ); ?></th>
						<td><?php self::eddap_post_type_slug_input(); ?></td>
					</tr>
					<tr>
						<th scope="row"><?php echo esc_html__( 'Archive slug', 'eddap' ); ?></th>
						<td><?php self::eddap_archive_slug_input(); ?></td>
					</tr>
					<tr>
						<th scope="row"><?php echo esc_html__( 'Category base', 'eddap' ); ?></th>
						<td><?php self::eddap_category_slug_input(); ?></td>
					</tr>
					<tr>
						<th scope="row"><?php echo esc_html__( 'Tag base', 'eddap' ); ?></th>
						<td><?php self::eddap_tag_slug_input(); ?></td>
					</tr>

				</tbody>
			</table>
			<?php wp_nonce_field( 'eddap-permalinks', 'eddap-permalinks-nonce' ); ?>

			<?php
		}

		/**
		 * Save the settings.
		 */
		public function settings_save() {
			if ( ! is_admin() ) {
				return;
			}
			if ( isset( $_POST['eddap-permalinks-nonce'] ) && ! wp_verify_nonce( $_POST['eddap-permalinks-nonce'], 'eddap-permalinks' ) ) {
			   exit;
			}

			if( isset( $_POST['eddap_category_slug'] ) )	{
				update_option( 'eddap_category_slug', sanitize_text_field( $_POST['eddap_category_slug'] ) );
			}
			if( isset( $_POST['eddap-structure'] ) )	{
				update_option( 'eddap-structure', sanitize_text_field( $_POST['eddap-structure'] ) );
			}
			if( isset( $_POST['eddap_tag_slug'] ) )	{
				update_option( 'eddap_tag_slug', sanitize_text_field( $_POST['eddap_tag_slug'] ) );
			}
			if( isset( $_POST['eddap_post_type_slug'] ) )	{
				update_option( 'eddap_post_type_slug', sanitize_text_field( $_POST['eddap_post_type_slug'] ) );
			}
			if( isset( $_POST['eddap_archive_slug'] ) )	{
				update_option( 'eddap_archive_slug', sanitize_text_field( $_POST['eddap_archive_slug'] ) );
			}
		}

		/**
		 * Adds plugin action links.
		 */
		function action_links( $links ) {
			$plugin_links = array(
				'<a href="options-permalink.php#eddap">' . esc_html__( 'Edit Permalinks', 'eddap' ) . '</a>',
			);
			return array_merge( $plugin_links, $links );
		}

		/**
		* Loading Text Domain for Internationalization
		*/
		function plugin_loaded_action() {
			load_plugin_textdomain( 'eddap', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
		}

		/**
		* Flush Permalink on Plugin Activation/Deactivation
		*/
		function permalink_flush() {
			flush_rewrite_rules();
		}

		/**
		 * Enqueue a script in the WordPress admin only on options-permalink.php page
		 *
		 */
		function enqueue_script( $hook ) {
		    if ( 'options-permalink.php' != $hook ) {
		        return;
		    }
		    wp_enqueue_script( 'eddap-admin', plugin_dir_url( __FILE__ ) . 'assets/js/admin.js', array('jquery'), '1.0' );
		}
	}

endif;

/**
* Plugin Initialize if Easy Digital Downloads Plugin Exists
*/
if ( !class_exists( 'Easy_Digital_Downloads' ) ) {
	add_action( 'admin_notices', 'eddap_inactive_notice_warn' );
} else {
	new EDDAP_Advanced_Permalinks();
}