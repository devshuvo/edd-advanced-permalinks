<?php
/**
 *	Class Including
 */
require_once( dirname( __FILE__ ) . '/class-admin-notice.php' );

/**
 * Review Notice
 */
function eddap_update_admin_notices($args){

	$remote_version = get_option( 'eddap_remote_version' );

	//! defined( 'ENVATO_MARKET_VERSION' )
	if( $remote_version && version_compare( EDDAP_VERSION, $remote_version, '<' ) ){

		$args[] = array(
			'id' => "eddap_updatenotices",
			'text' => '<p>There is a new version of EDD Advanced Permalinks available. Install/Activate <a href="https://envato.com/market-plugin/" target="_blank">Envato Market WordPress Plugin</a> to receive updates('.$remote_version.') automatically. <br> See <a href="https://www.wpexplorer.com/envato-market-plugin-guide/" target="_blank">More about envato plugins automatic updates</a></p>',
			'logo' => "https://s3.envato.com/files/281875486/thumbnail.png",
			'border_color' => "#ffb900",
			'is_dismissable' => "false",
			'dismiss_text' => "",
			'buttons' => array(
				array(
					'text' => "Download Envato Market WordPress Plugin",
					'link' => "https://envato.github.io/wp-envato-market/dist/envato-market.zip",
					'target' => "_blank",
					'icon' => "dashicons dashicons-external",
					'class' => "button-secondary",
				),
				array(
					'text' => "Read Update Tutorial",
					'link' => "https://www.wpexplorer.com/envato-market-plugin-guide/",
					'target' => "_blank",
					'icon' => "dashicons dashicons-external",
					'class' => "button-secondary",
				),
			)
		);
	}

	$args[] = array(
		'id' => "eddap_reviewnotices",
		'text' => "We hope you're enjoying this plugin! Could you please give a 5-star rating on WordPress to inspire us?",
		'logo' => "https://s3.envato.com/files/281875486/thumbnail.png",
		'border_color' => "#000",
		'is_dismissable' => "true",
		'dismiss_text' => "Dismiss",
		'buttons' => array(
			array(
				'text' => "Ok, you deserve it!",
				'link' => "https://codecanyon.net/downloads",
				'target' => "_blank",
				'icon' => "dashicons dashicons-external",
				'class' => "button-secondary",
			),
		)
	);

	return $args;
}
add_filter( 'addonmaster_admin_notice', 'eddap_update_admin_notices' );


/**
 * Add plugin update msg
 *
 */
function eddap_plugin_update_check_msg( $links ) {

	if ( !is_admin() ) {
		return;
	}

	$html = "";

	$remote_version = get_option( 'eddap_remote_version' );

	//! defined( 'ENVATO_MARKET_VERSION' )
	if( $remote_version && version_compare( EDDAP_VERSION, $remote_version, '<' ) ){
		$html = '<tr class="plugin-update-tr active">
		    <td colspan="3" class="plugin-update colspanchange">
		        <div class="update-message notice inline notice-warning notice-alt">
		            <p>There is a new version of EDD Advanced Permalinks available. Install/Activate <a href="https://envato.com/market-plugin/" target="_blank">Envato Market WordPress Plugin</a> to receive updates('.$remote_version.') automatically. <br> See <a href="https://www.wpexplorer.com/envato-market-plugin-guide/" target="_blank">More about envato plugins automatic updates</a></p>
		        </div>
		    </td>
		</tr>';
	}

	echo $html;

}
add_filter( 'after_plugin_row_edd-advanced-permalinks/edd-advanced-permalinks.php', 'eddap_plugin_update_check_msg' );

// remote update check
function eddap_plugin_update_check_remote_function() {

	$response = wp_remote_get( 'https://raw.githubusercontent.com/akshuvo/versions/master/eddap.json', array(
		'timeout' => 30,
		'headers' => array(
			'Accept' => 'application/json'
		) )
	);

	if ( is_array( $response ) && ! is_wp_error( $response ) ) {

		$body = wp_remote_retrieve_body( $response );
		$data = json_decode( $body );
		$remote_version = $data->version;

		update_option( 'eddap_remote_version', $remote_version );
	}
}

// Set transient for update check
if ( ! get_transient( 'eddap_plugin_update_check' ) ) {
    set_transient( 'eddap_plugin_update_check', true, 30 * MINUTE_IN_SECONDS );

    // It's better use a hook to call a function in the plugin/theme
    add_action( 'init', 'eddap_plugin_update_check_remote_function' );
}