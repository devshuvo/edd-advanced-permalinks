<?php
/**
 * EDD Plugin inactive Notice
 */
function eddap_inactive_notice_warn() {
	?>
	<div class="notice notice-warning is-dismissible">
	    <p>
	    	<strong><?php echo esc_html__( 'EDD Advanced Permalinks requires Easy Digital Downloads to be activated ', 'eddap' ); ?> <a href="<?php echo esc_url( admin_url('/plugin-install.php?s=slug:easy-digital-downloads&tab=search&type=term') ); ?>"><?php echo esc_html__('Install Now','eddap'); ?></a></strong>
	    </p>
	</div>
	<?php
}
