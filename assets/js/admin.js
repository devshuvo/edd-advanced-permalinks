(function($) {
	"use strict";
	$(document).ready(function(){
		$('.eddap-structure-list input').change(function() {
			var edd_slug_val = null;

			if ( $(this).val() == 'post_name' ) {
				edd_slug_val = "item/%postname%";
			} else if ( $(this).val() == 'post_id' ) {
				edd_slug_val = "item/%post_id%";
			} else if ( $(this).val() == 'post_name_id' ) {
				edd_slug_val = "item/%postname%/%post_id%";
			} else if ( $(this).val() == 'post_cat' ) {
				edd_slug_val = "item/%download_category%/%postname%";
			} else if ( $(this).val() == 'post_author' ) {
				edd_slug_val = "item/%author%/%postname%";
			} else {
				//nothing
			}

			if( edd_slug_val ){
				$('input[name="eddap_post_type_slug"]').val( edd_slug_val );
			}
		});
	});
})(jQuery);